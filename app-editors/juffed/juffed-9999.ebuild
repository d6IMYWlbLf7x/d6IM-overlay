# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit cmake-utils git-r3

EGIT_REPO_URI="https://github.com/Mezomish/${PN}.git"

DESCRIPTION="QScintilla-based tabbed text editor with syntax highlighting (Qt5 version)"
HOMEPAGE="http://juffed.com/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~ppc64 ~x86"
IUSE="debug"

RDEPEND="
	app-i18n/enca
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	x11-libs/qscintilla:=[qt5(-)]
"
# removed:
#	dev-qt/qtsingleapplication[qt5(+),X]


DEPEND="${RDEPEND}"

DOCS=( ChangeLog README )

src_prepare() {
	cmake-utils_src_prepare

	sed -i -e '/set(CMAKE_CXX_FLAGS/d' CMakeLists.txt || die

	# fix for the script that sometimes fails to find the Qt5 version of QScintilla
	sed -i -e 's/qscintilla2-qt5)/qscintilla2-qt5 qscintilla2 qscintilla2_qt5)/g' cmake/FindQScintilla2.cmake || die
}

src_configure() {
	local mycmakeargs=(
		-DUSE_QT5=ON
		# QtSingleApplication breaks juffed if its compiled with both Qt4 and Qt5 support.
		#  (causes juffed to link with both Qt versions which results in a crash at startup.)
		-DUSE_SYSTEM_QTSINGLEAPPLICATION=OFF
		-DUSE_ENCA=ON
	)
	cmake-utils_src_configure
}
