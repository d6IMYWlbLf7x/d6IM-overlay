# Copyright (c) 2016  d6IMYWlbLf7x <wDUegt3uzgPo0s@tutanota.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

EAPI="6"
DESCRIPTION="A collection of various POSIX shell scripts."
HOMEPAGE="https://github.com/d6IMYWlbLf7x/${PN}"
SRC_URI=""
SLOT="0"
IUSE="+general +gentoo"
LICENSE="GPL-3+"
KEYWORDS="~*"
DEPEND="
	sys-devel/make
	sys-apps/coreutils
"
RDEPEND="
	sys-apps/coreutils
	virtual/awk
	sys-apps/grep
	general? ( sys-apps/findutils )
	gentoo? ( sys-apps/portage )
"
REQUIRED_USE="|| ( general gentoo )"

inherit git-r3

EGIT_REPO_URI="https://github.com/d6IMYWlbLf7x/${PN}.git"

do_subdir_install() {
	if use "${1}" ; then
		cd "${S}/${1}/"
		emake PREFIX="${D}" DESTDIR="/usr/" install || die "emake install failed."
	fi
}

src_install() {
	dodoc README.md
	do_subdir_install general
	do_subdir_install gentoo
}
